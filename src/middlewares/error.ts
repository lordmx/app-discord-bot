import { Middleware, KoaMiddlewareInterface } from "routing-controllers";
import { Context } from "koa";
import { AuthorizationRequiredError } from "routing-controllers/error/AuthorizationRequiredError";

@Middleware({ type: 'before', priority: 40 })
export class ErrorHandler implements KoaMiddlewareInterface {
    async use(context: Context, next: (err?: any) => Promise<any>): Promise<any> {
        return await next().catch((err) => {
            console.error('Error name: ', err.constructor.name);

            switch (err.constructor.name) {
                case 'AuthorizationRequiredError':
                case 'UnauthorizedError':
                    context.throw(401, 'Unauthorized');
                    break;

                default:
                    console.error('Error: ', err);
                    context.throw(err);
            }
        });
    }
}