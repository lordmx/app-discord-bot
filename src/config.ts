import { Service } from "typedi/decorators/Service";

@Service()
export class Config {
    host: string = process.env.HOST;
    port: number = Number.parseInt(process.env.PORT);
    discord = {
        token: process.env.DISCORD_TOKEN,
        prefix: process.env.DISCORD_PREFIX || '+',
        channel: process.env.DISCORD_CHANNEL,
    };
}
