import 'dotenv/config';
import 'source-map-support/register';
import "reflect-metadata"

import { createKoaServer, useContainer } from "routing-controllers";
import { Container } from "typedi";
import { Server } from "net";
import { Config } from "./config";

// middlewares
import { ErrorHandler } from "./middlewares/error";
import { RequestLogger } from "./middlewares/logger";

// controllers
import { MessagesController } from "./controllers/messages";
import {Discord} from "./infrastructure/discord";

useContainer(Container);
const config: Config = Container.get(Config);

Container.get(Discord);

const koaApp = createKoaServer({
    routePrefix: '/api/v1',
    cors: true,
    controllers: [
        MessagesController,
    ],
    middlewares: [
        ErrorHandler,
        RequestLogger,
    ],
});

const server: Server = koaApp.listen(config.port, config.host);

server.on('error', (error: string) => {
    console.log('connection error', error);
});
