import {
    Client,
    Message,
    Channel,
    TextChannel
} from 'discord.js';
import { Service } from 'typedi';
import { Config } from "../config";

@Service()
export class Discord {
    private client: Client;

    /**
     * @param {Config} config
     */
    constructor(private config: Config) {
        this.client = new Client();

        this.client.on('ready', async () => {
            console.info(`[Discord] Ready. Logged in as ${this.client.user.tag}!`);
            this.client.user.setActivity(`Serving ${this.client.guilds.size} servers`);
        });

        this.client.on('error', async (err) => {
            console.error('[Discord] Error: ', err);
            process.exit(1);
        });


        this.client.login(config.discord.token);
    }

    /**
     * @param {string} to
     * @param {string} message
     * @return {Promise<Message>}
     */
    async send(to: string, message: string): Promise<Message> {
        const channel: Channel = this.client.channels.get(to);

        console.info(`[Discord] Sending to channel "${to}": `, message);

        if (channel) {
            return <Promise<Message>>(<TextChannel>channel).send(message);
        }

        return Promise.resolve(null);
    }
}