import {
    JsonController,
    Post,
    Get,
    BodyParam,
    Ctx,
    BadRequestError,
    QueryParam,
} from "routing-controllers";
import { Service } from "typedi";
import { Context } from "koa";
import { Discord } from "../infrastructure/discord";
import { Config } from "../config";

@Service()
@JsonController()
export class MessagesController {
    /**
     * @param {Discord} discord
     * @param {Config} Config
     */
    constructor(
        private discord: Discord,
        private config: Config,
    ) {

    }

    /**
     * @param {String} message
     * @param {Context} ctx
     */
    @Post("/messages")
    sendFromPost(
        @BodyParam('message') message: string,
        @Ctx() ctx: Context,
    ): Promise<any> {
        return new Promise<any>((resolve) => {
            if (!message) {
                throw new BadRequestError('Empty message');
            }

            this.discord.send(this.config.discord.channel, message);

            ctx.status = 201;
            resolve({});
        });
    }

    /**
     * @param {String} message
     * @param {Context} ctx
     */
    @Get("/messages")
    sendFromGet(
        @QueryParam('message') message: string,
        @Ctx() ctx: Context,
    ): Promise<any> {
        return new Promise<any>((resolve) => {
            if (!message) {
                throw new BadRequestError('Empty message');
            }

            this.discord.send(this.config.discord.channel, message);

            ctx.status = 201;
            resolve({});
        });
    }
}